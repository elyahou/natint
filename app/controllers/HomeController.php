<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function index()
	{
		if (Input::has('ajax'))
		{
			$layout = 'ajax';
		}
		else
		{
			$layout = 'layout';
		}

		$sort = Input::get('sort', 'id');

		$filter = Input::get('filter', Session::get('filter'));

		if ($filter)
		{
			Session::put('filter', $filter);
			$cars = Car::orderBy($sort)->whereContinent($filter)->get();
		}
		else
		{
			$cars = Car::orderBy($sort)->get();
		}
		

		return View::make('cars', compact('cars', 'layout'));
	}

}
