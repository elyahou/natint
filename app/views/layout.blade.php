<!DOCTYPE html>
<html>
<head>
    <title>cars</title>
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <link rel="stylesheet" href="http://cdn.jsdelivr.net/qtip2/2.2.1/jquery.qtip.min.css">
    <script src="http://cdn.jsdelivr.net/qtip2/2.2.1/jquery.qtip.min.js"></script>

    <style>
        th
        {
            background-color: blue;
        }
        table
        {
            border-spacing: 0;
            border-collapse: separate;
        }
        td,th
        {
            padding: 5px 3px 5px 5px;
            text-align: left;
            border: solid 1px black;
            border-radius: 5px;
        }
        tr:nth-child(even) {
           background-color: grey;
        }

        .green td
        {
            background-color: green;
        }

        tr:hover td
        {
            background-color: yellow
        }
    </style>
</head>
<body>
<a href="#" class="filter" id="asia">Show only Asian Cars</a>
<a href="#" class="filter" id="america">Show only American Cars</a>
@yield("content")    
<script>
$( document ).ready( function()
{
    $(".filter").on("click", function()
    {
        $("#load").load("cars?filter="+$(this).attr("id")+"&ajax=1");
    });
    $('[title!=""]').qtip();
} );
        
    </script>       
</body>
</html>