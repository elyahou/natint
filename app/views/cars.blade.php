@extends($layout)

@section("content")
<table id="load">
    <thead>
        <tr>
            <th><a href="/cars?sort=pos">Pos#</a></th>
            <th><a href="/cars?sort=model">Model</a></th>
            <th><a href="/cars?sort=price">Price</a></th>
            <th>Image</th>
        </tr>
    </thead>
    <tbody>
    @foreach ($cars as $car)
        <tr class="{{ ($car->model == "") ? "green": "" }}" >
            <td>
                @if ($car->pos > 0)
                    {{$car->pos}}
                @endif
            </td>
            <td title="{{$car->continent}}">{{$car->model}}</td>
            <td>{{number_format($car->price, 0, ".", ",")}}</td>
            <td>{{$car->image}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
@stop