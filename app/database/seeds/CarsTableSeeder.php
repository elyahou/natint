<?php

class CarsTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('cars')->truncate();

		$cars = array(
            array(
                "model" => "Opel Corsa",
                "price" => 70000,
                "image" => "",
                "pos"   => 1,
                "continent" => ""
                ),
            array(
                "model" => "Mitzubishi Lancer",
                "price" => 100000,
                "image" => "",
                "pos"   => 2,
                "continent" => "asia"
                ),
            array(
                "model" => "Mazda 3",
                "price" => 120000,
                "image" => "",
                "pos"   => 3,
                "continent" => "asia"
                ),
            array(
                "model" => "Honda Cisic",
                "price" => 70000,
                "image" => "",
                "pos"   => 4,
                "continent" => "asia"
                ),
            array(
                "model" => "Volvo",
                "price" => 180000,
                "image" => "",
                "pos"   => 5,
                "continent" => ""
                ),
            array(
                "model" => "Toyota Corola",
                "price" => 125000,
                "image" => "",
                "pos"   => 6,
                "continent" => "asia"
                ),
            array(
                "model" => "",
                "price" => 675000,
                "image" => "",
                "pos"   => 0,
                "continent" => ""
                ),
		);

		// Uncomment the below to run the seeder
		 DB::table('cars')->insert($cars);
	}

}
